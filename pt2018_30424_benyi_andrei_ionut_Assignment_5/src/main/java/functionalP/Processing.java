package functionalP;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Processing {
	
	public static MonitoredData dateProc(String s) {
		String activity;
		
		String st,f;
		Date startDate, endDate;
		startDate=null;
		endDate=null;
		SimpleDateFormat f2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		st=s.substring(0,19);
		try {
			startDate=f2.parse(st);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		f=s.substring(21,40);
		try {
			endDate=f2.parse(f);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		activity=s.substring(42);
		
		MonitoredData rezm=new MonitoredData(startDate,endDate,activity);
		
		return rezm;
	}
	
	public static void main(String[] args) {
		Daily dObj=new Daily();
		List<MonitoredData> l = new ArrayList<MonitoredData>();
		Map<String, Long> mapAct =new HashMap<String,Long>();
		
			//read from file
			try(Stream<String> stream=Files.lines(Paths.get("Activities.txt"))){
				stream.forEach((p)->l.add(dateProc(p)));
			}
			catch(IOException e){
				e.printStackTrace();
			}
			
			Map<Integer, Map<String,Long>> mapTask=dObj.dailyAct(l);
			FileWriter fw2=null;
			BufferedWriter bw2=null;
			
			try {
					fw2 = new FileWriter("ocr.txt");
					bw2 = new BufferedWriter(fw2);
					for(int i=1;i<mapTask.size();i++) {
						Map<String, Long> m2=mapTask.get(i);
						Set set = m2.entrySet();
						Iterator it=set.iterator();
						bw2.write("Day "+i);
						bw2.newLine();
						while(it.hasNext()) {
							Map.Entry mentry = (Map.Entry)it.next();
							bw2.write("Occurrence of "+mentry.getKey()+": "+mentry.getValue());
							bw2.newLine();
						}
						bw2.newLine();
					}
			}catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if(bw2!=null)
						bw2.close();
					if(fw2!=null)
						fw2.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			dObj.calculateActiv10(l);
			
			List<String> proc90=dObj.compProcents( dObj.atMost5Min(l),dObj.countOccurrences(l));
			dObj.printRes90(proc90);
			
	}

}

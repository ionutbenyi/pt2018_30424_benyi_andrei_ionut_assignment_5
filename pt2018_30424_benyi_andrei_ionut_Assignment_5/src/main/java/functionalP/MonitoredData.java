package functionalP;
import java.util.*;
import java.util.concurrent.TimeUnit;
public class MonitoredData {
	
	private Date startTime;
	private Date endTime;
	private String activity;
	
	public MonitoredData(Date st, Date end, String activ) {
		this.startTime=st;
		this.endTime=end;
		this.activity=activ;
	}
	
	public Date getStartTime() {
		return this.startTime;
	}
	
	public Date getEndTime() {
		return this.endTime;
	}
	
	public String getActivity() {
		return this.activity;
	}
	
	public long getDurationM() {
		return TimeUnit.MILLISECONDS.toMinutes(endTime.getTime()-startTime.getTime());
	}
	
	public long getDurationH() {
		return TimeUnit.MILLISECONDS.toHours(endTime.getTime()-startTime.getTime());
	}
}

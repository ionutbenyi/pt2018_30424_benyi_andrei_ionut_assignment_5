package functionalP;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class Daily {
	
	//we build the day array in order to use its elements as keys in the daily map
	public static List<Date> buildDays(List<MonitoredData> a) {
		List<Date> daysArr=new ArrayList<Date>();
		Date stAux=a.get(0).getStartTime();
		Date endAux=a.get(0).getEndTime();
		daysArr.add(a.get(0).getStartTime());
		
		for(MonitoredData d : a) {
			if(d.getStartTime().getMonth()!=stAux.getMonth() || d.getStartTime().getDay()!=stAux.getDay()) {
				daysArr.add(d.getStartTime());
			}
			stAux=d.getStartTime();
			endAux=d.getEndTime();
		}
		return daysArr;
	}

	//the stream based method which counts the occurrences of each activity
	public static Map<String, Long> countOccurrences(List<MonitoredData> a){
		Map<String,Long> m =new HashMap<String,Long>();
		m=a.stream()
		.map(x -> x.getActivity())
		.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		return m;
	}
	
	//the stream based method which counts the days
	public static long countDays(List<MonitoredData> a) {
		long days=a.stream().map(str->str.getEndTime().getDate()).distinct().count();
		return days;
	}
	
	//the daily occurrence of activities
	public Map<Integer, Map<String, Long>> dailyAct(List<MonitoredData> m){
		
		List<Date> daysArr=buildDays(m);
		
		Map<String,Long> hmap =new HashMap<String,Long>();
		Map<Integer,Map<String,Long>> mmap =new HashMap<Integer,Map<String,Long>>();
		List<MonitoredData> listD=new ArrayList<MonitoredData>();
		int ct=0;
		
		for(Date dx: daysArr) {
			ArrayList<MonitoredData> al=new ArrayList<MonitoredData>();
			for(MonitoredData dm:m) {
				if(dm.getStartTime().getDay()==dx.getDay() && dm.getStartTime().getMonth()==dx.getMonth())
					al.add(dm);
			}
			
			mmap.put(ct,countOccurrences(al));
			ct++;
		}
		
		return mmap;
	}
	
	//retunrs a map which holds the total duration in hours of each activity
	public Map<String, Long> totalTest(List<MonitoredData> l ){
		Map<String,Long> m =new HashMap<String,Long>();
		m=l.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,
						Collectors.summingLong(MonitoredData::getDurationH)));
				
		return m;
	}
	
	//activities >=10hr + writing in file
	public void calculateActiv10(List<MonitoredData> l) {
		Map<String,Long> mp=totalTest(l);
		FileWriter fw2=null;
		BufferedWriter bw2=null;
		try {
			fw2 = new FileWriter("daily10.txt");
			bw2 = new BufferedWriter(fw2);
			//collect the activities with >=10 hrs activity time
			Map<String, Long> collect = mp.entrySet().stream()
					.filter(map -> map.getValue() >= 10)
					.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		
			Set set = collect.entrySet();
			Iterator it=set.iterator();
			while(it.hasNext()) {
				Map.Entry mentry = (Map.Entry)it.next();
				bw2.write("Total of "+mentry.getKey()+": "+mentry.getValue());
				bw2.newLine();
			}
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(bw2!=null)
					bw2.close();
				if(fw2!=null)
					fw2.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//retunrs a map which holds the total duration in minutes of each activity
	public Map<String, Long> totalActiv5(List<MonitoredData> l ){
		Map<String,Long> m =new HashMap<String,Long>();
		m=l.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,
						Collectors.summingLong(MonitoredData::getDurationM)));
				
		return m;
	}
	
	public Map<String,Long> atMost5Min(List<MonitoredData> l){
		Map<String,Long> mp=totalActiv5(l);
			//collect the activities with >=10 hrs activity time
			Map<String, Long> res = mp.entrySet().stream()
					.filter(map -> map.getValue() <= 5)
					.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		return res;	
	}
	
	//get the activities which have a >90% rate of 5 minutes per activity
	public List<String> compProcents(Map<String, Long> map, Map<String,Long> part){
		Map<String,Long> res=map.entrySet().stream()
				.filter(mp -> 90*mp.getValue()/100<part.get(mp.getKey()))
				.collect(Collectors.toMap(p -> p.getKey(), p ->p.getValue()));
				
		List<String> rez=new ArrayList(res.keySet());
		
		return rez;
	}
	
	public void printRes90(List<String> proc90) {
		FileWriter fw3=null;
		BufferedWriter bw3=null;
		try {
			fw3 = new FileWriter("test90Percent.txt");
			bw3 = new BufferedWriter(fw3);
			bw3.write("The following have 90% of time a duration of 5 min");
			bw3.newLine();
			for(String rl: proc90) {
				bw3.write(rl);
				bw3.newLine();
			}
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(bw3!=null)
					bw3.close();
				if(fw3!=null)
					fw3.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}
